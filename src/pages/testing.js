import React from "react";
import { Link } from "gatsby";

export default () => (
  <div>
    <Link to="/">Back to home.</Link>
    <p>Testing.</p>
  </div>
);

import React from "react";
import { Link } from "gatsby";
import Header from "../components/header.js";

export default () => (
  <div>
    <Header text="Header!" />
    <Link to="/testing/">A testing page</Link>
    <p>
      It's just Pollen with a different syntax and a different file structure,
      really.
    </p>
  </div>
);
